using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlThomas : MonoBehaviour
{
    private GameObject jugador;
    public int rapidez;

    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
