using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlLaura : MonoBehaviour
{
    private GameObject jugador;
    public float rapidez;
    bool tengoQueBajar = false;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        if (transform.position.z >= 6)
        {
            tengoQueBajar = true;
        }
        if (transform.position.z <= -5)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            Destroy(gameObject);
        }
    }

    private void Subir()
    {
        transform.position += new Vector3(0, 0, 1) * rapidez * Time.deltaTime;
    }

    private void Bajar()
    {
        transform.position -= new Vector3(0, 0, 1)  * rapidez * Time.deltaTime;
    }

}




