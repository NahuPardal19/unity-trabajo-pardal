
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJohn : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    private bool tengoQueBajar = false;
    private Rigidbody rb;




    void Start()
    {
        rb = GetComponent<Rigidbody>();
       
        hp = 50;
        jugador = GameObject.Find("Jugador");

    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

        if (transform.position.y >= 8)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 2)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }


    public void recibirDaņo()
    {
        hp = hp - 25;


        if (hp <= 0)
        {

            this.desaparecer();
        }
      
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaņo();
        }
        else if (collision.gameObject.CompareTag("Jugador"))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }



}