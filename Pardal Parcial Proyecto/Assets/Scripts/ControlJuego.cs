using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public GameObject bot;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    private List<GameObject> listaObjetos = new List<GameObject>();
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;
    public GameObject obj;
    public GameObject obj1;
    public GameObject obj2;
    public GameObject obj3;
    private int cont;
    public GameObject[] gameObjects;

    void Start()
    {
        cont = 0;
        ComenzarJuego();
    }

    void Update()
    {
        
        gameObjects = GameObject.FindGameObjectsWithTag("Enemigo1");

        if (gameObjects.Length == 6 && cont == 0)
        {
            listaObjetos.Add(Instantiate(obj, new Vector3(1f, 5f, -0.20f), Quaternion.identity));
            cont = 1;
        }
        if (gameObjects.Length == 4 && cont == 1)
        {
            listaObjetos.Add(Instantiate(obj1, new Vector3(44f, 5f, -0.20f), Quaternion.identity));
            cont = 2;
        }
        if (gameObjects.Length == 3 && cont == 2)
        {
            listaObjetos.Add(Instantiate(obj2, new Vector3(94.85f, 16f, 0f), Quaternion.identity));
            cont = 3;
        }
        if (gameObjects.Length == 0 && cont == 3)
        {
            listaObjetos.Add(Instantiate(obj3, new Vector3(135f, 12f, 0f), Quaternion.identity));
            cont = 4;
        }

        if (tiempoRestante == 0)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    void ComenzarJuego()
    {


        foreach (GameObject item in listaEnemigos)
        {
            Destroy(item);
        }

        listaEnemigos.Add(Instantiate(bot, new Vector3(10f, 0.7f, 10f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(10f, 0.7f, 0f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(10f, 0.7f, -10f), Quaternion.identity));
        StartCoroutine(ComenzarCronometro(60));

    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Tiempo: " + tiempoRestante.ToString();
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}
    
