using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public Camera camaraPrimeraPersona;
    public GameObject proyectil;
    public GameObject jugador;
    public GameObject pared;
    public GameObject paredmov;
    public GameObject obj;
    private bool dobleSalto;
    float tiempoRestante;
    public GameObject thomas;
    private List<GameObject> listaObjetos = new List<GameObject>();

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        dobleSalto = false;
        

    }

    private void setearTextos()
    {
        
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 11 )
        {
            SceneManager.LoadScene("Victoria"); ;
            rapidezDesplazamiento = 13.0f;
        }
        else if (cont == 1)
        {
            pared.gameObject.SetActive(false);
            rapidezDesplazamiento = 13.0f;
        }
        else if (cont == 2)
        {
            magnitudSalto = 13.0f;
            paredmov.gameObject.SetActive(false);
        }
        else if (cont == 3)
        {
            magnitudSalto = 7f;
            rapidezDesplazamiento = 5f;
            transform.localScale = new Vector3(5f, 5f, 5f);
            paredmov.gameObject.SetActive(false);
        }
        else if (cont == 4)
        {
            transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }

        else if (cont == 5)
        {
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            rapidezDesplazamiento = 10f;
            magnitudSalto = 7f;
        }

        else if (cont == 10)
        {
            thomas.gameObject.SetActive(false);
            listaObjetos.Add(Instantiate(obj, new Vector3(185f, 16f, -4f), Quaternion.identity));
        }
    }

    


   void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

     
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }
            else if (dobleSalto)
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                rb.velocity = Vector3.zero;
                dobleSalto = false;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 5);

        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (EstaEnPiso())
        {
            dobleSalto = true;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("piso") == true)
        {
            SceneManager.LoadScene(0);
        }

    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

 


}
